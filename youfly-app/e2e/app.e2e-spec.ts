import { YouflyAppPage } from './app.po';

describe('youfly-app App', () => {
  let page: YouflyAppPage;

  beforeEach(() => {
    page = new YouflyAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
