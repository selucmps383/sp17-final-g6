import { AngularAirportPage } from './app.po';

describe('angular-airport App', function() {
  let page: AngularAirportPage;

  beforeEach(() => {
    page = new AngularAirportPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
