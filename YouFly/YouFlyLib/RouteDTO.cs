﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YouFlyLib
{
    class RouteDTO
    {
        public int RtID { get; set; }
        public string Airport { get; set; }//this needs to be an airport
        public string Destination { get; set; }
        public string RouteCode { get; set; }
    }
}
