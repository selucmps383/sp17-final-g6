﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YouFlyLib
{
    public class FlightsDTO
    {
        public int ID { get; set; }
        public int Price { get; set; }
        public int RegSeats { get; set; }
        public int FCSeats { get; set; }
        public string Location { get; set; }
        public string Destination { get; set; }
    }
}
