﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YouFlyLib
{
    public class SeatDTO
    {
        public bool Open { get; set; }
        public bool FirstClass { get; set; }
    }
}
