﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YouFlyLib
{
    public class AirportDTO
    {
        public int AirportID { get; set; }
        public string AirportName { get; set; }
        public string AirportCity { get; set; }
        public string AirportCountry { get; set; }
        public string AirportIATA { get; set; }
        public string AirportICAO { get; set; }
        public float AirportLong { get; set; }
        public float AirportLat { get; set; }
        public float AirportAlt { get; set; }
        public float AirportUTC { get; set; }
        public string AirportTimezone { get; set; }
        public string AirportDST { get; set; }
    }
}
