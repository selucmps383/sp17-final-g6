﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using YouFly.Models;



namespace YouFly.Data
{
    public static class DbInitializer
    {
        public static void Initialize(FlightContext context)

        {
            

            context.Database.EnsureCreated();

            // Look for any user.
            if (context.UserAccounts.Any() && context.Airports.Any())
            {
                
                context.SaveChanges();
                return;   // DB has been seeded
            }
            var useraccounts = new UserAccount[]
            {
                new UserAccount{Name="Neil Hoang",Email="admin@selu.edu",UserName="admin",Password="password"}
             
            };
            foreach (UserAccount ua in useraccounts)
            {
                context.UserAccounts.Add(ua);
            }
                context.SaveChanges();

            TextReader reader = File.OpenText("Data/airports.csv");

            //var airports = new List<Airport>();
            try
            {
                var csv = new CsvReader(reader);
                csv.Configuration.Delimiter = ",";
                var airports = csv.GetRecords<Airport>();
 
                foreach (Airport ap in airports)
                {
                    context.Airports.Add(ap);
                }
            }
            catch(CsvHelperException ex)
            {
                Console.WriteLine(ex.Data.Values);
            }
                context.SaveChanges();
        }
    }
}
