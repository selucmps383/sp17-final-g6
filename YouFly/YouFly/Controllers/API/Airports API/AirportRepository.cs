﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouFly.Models;

namespace YouFly.Controllers.API
{
    public class AirportRepository : IAirportRepository
    {
        private  FlightContext _context;

        public AirportRepository(FlightContext context)
        {
            _context = context;
        }
        public Flights Get(int id)
        {
           return _context.Flights.FirstOrDefault(w => w.ID == id);
        }
        
        public IEnumerable<Flights> GetAll()
        {
            return _context.Flights;
        }

        public void update()
        {
            _context.SaveChanges();
        }
    }
}
