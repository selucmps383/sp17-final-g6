﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouFly.Controllers.Helpers;
using YouFly.Models;
using YouFlyLib;

namespace YouFly.Controllers.API
{
    [Route("api/api")]
    public class AirportsController
    {
        private IAirportRepository _apRepo;

        public AirportsController(IAirportRepository apRepo)
        {
            _apRepo = apRepo;
        }

        [HttpGet("GetAll")]
        public IEnumerable<FlightsDTO> GetAllFlights()
        {
            DTOConverterHelper dch = new DTOConverterHelper();
            List<FlightsDTO> temp = new List<FlightsDTO>();
            foreach (Flights f in _apRepo.GetAll())
            {
                temp.Add(dch.Convert(f));
            }
            return temp;
            
        }
        [HttpPost("Book/{id}/{FC}")]
        public bool BookFlight(int id, bool FC)
        {
            if (_apRepo.Get(id).TotalSeat > 0 && FC == false)
            {
                _apRepo.Get(id).TotalSeat = _apRepo.Get(id).TotalSeat -1;
                _apRepo.update();
                return true;
            }
            if (_apRepo.Get(id).TotalFC > 0 && FC == true)
            {
                _apRepo.Get(id).TotalFC = _apRepo.Get(id).TotalFC - 1;
                _apRepo.update();
                return true;
            }
            return false;

        }
        
        }
}
    