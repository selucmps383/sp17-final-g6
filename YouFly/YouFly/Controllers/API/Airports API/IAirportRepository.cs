﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouFly.Models;
using YouFlyLib;

namespace YouFly.Controllers.API
{
    public interface IAirportRepository
    {
        Flights Get(int id);
        IEnumerable<Flights> GetAll();
        void update();
        
    }
}
