﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using YouFly.Models;
using YouFlyLib;

namespace YouFly.Controllers.Helpers
{
    public class DTOConverterHelper
    {
        public AirportDTO Convert(Airport ap)
        {
            return new AirportDTO
            {
                AirportAlt = ap.Altitude,
                AirportCity=ap.City,
                AirportCountry= ap.Country,
                AirportDST=ap.DST,
                AirportIATA=ap.IATA,
                AirportICAO=ap.ICAO,
                AirportID=ap.ID,
                AirportLat=ap.Latitude,
                AirportLong=ap.Longtitude,
                AirportName=ap.Name,
                AirportTimezone=ap.TimeZone,
                AirportUTC=ap.UTC
            };
        }

        public FlightsDTO Convert(Flights fl)
        {
 
            return new FlightsDTO
            {
                Destination = fl.Destination,
                ID = fl.ID,
                Location = fl.From,
                Price = fl.Price,
                RegSeats=fl.TotalSeat,
                FCSeats=fl.TotalFC
                

            };
        }


    }
}
