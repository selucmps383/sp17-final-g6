﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using YouFly.Data;
using YouFly.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace YouFly.Controllers
{
    public class RouteController : Controller
    {
        private readonly FlightContext _context;

        public RouteController(FlightContext context)
        {
            _context = context;
        }
        // GET: /<asdf>/
        public async Task<IActionResult> Index(string airportIATA)
        {
            // Use LINQ to get list of IATA.
            IQueryable<string> iataQuery = from a in _context.Airports
                                           orderby a.IATA
                                           select a.IATA;

            var airports = from a in _context.Airports
                           select a;

            if (!String.IsNullOrEmpty(airportIATA))
            {
                airports = airports.Where(x => x.IATA == airportIATA);
            }
            var airportIATAVM = new AirportIATAViewModel();
            airportIATAVM.iata = new SelectList(await iataQuery.Distinct().ToListAsync());
            airportIATAVM.airports = await airports.ToListAsync();
            ViewBag.IATA = airportIATAVM.iata;
            ViewBag.airports = airports;
            
            return View(_context.Flights);
        }
        
        public async Task<IActionResult> Create()
        {
            IQueryable<string> iataQuery = from a in _context.Airports
                                           orderby a.IATA
                                           select a.IATA;

            var airports = from a in _context.Airports
                           select a;
            var airportIATAVM = new AirportIATAViewModel();
            airportIATAVM.iata = new SelectList(await iataQuery.Distinct().ToListAsync());
            airportIATAVM.airports = await airports.ToListAsync();
            ViewBag.IATA = airportIATAVM.iata;
            ViewBag.airports = airports;

            return View();
        }
        // POST: UserAccounts/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IATAF,IATAT,Date,FlightTime,Distance,FCSeats,RegSeats,PriceFC,Price")] Route route)
        {
           
            if (ModelState.IsValid)
            {
               
                Flights temp = new Flights
                {
                    From = _context.Airports.FirstOrDefault(s => s.IATA == route.IATAF).Name,
                    Destination = _context.Airports.FirstOrDefault(s => s.IATA == route.IATAT).Name,
                    Price = route.Price,
                    PriceFC = route.PriceFC,
                    TotalFC = route.FCSeats,
                    TotalSeat = route.RegSeats,
                    Date = route.Date,
                    time = route.FlightTime
                };
                _context.Flights.Add(temp);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(route);
        }

    }
}