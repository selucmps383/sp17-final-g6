using Microsoft.AspNetCore.Mvc;

namespace YouFly.Controllers
{
    public class AdminController : Controller
    {
        public IActionResult Index()
        {
            return Content("You are in the Admin Index View!");
        }
        public IActionResult Other()
        {
            return View("You are in the Admin Other View!");
        }
    }
}