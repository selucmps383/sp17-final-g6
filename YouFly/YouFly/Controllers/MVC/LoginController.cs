﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace YouFly.Controllers
{
    public class LoginController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return new ContentResult { Content = "Admin Login" };
            //return View();
        }

        public IActionResult Login()
        {
            return new ContentResult { Content = "Login:" };
        }

        public IActionResult Logout()
        {
            return new ContentResult { Content = "Logout" };
        }

      /*  public IActionResult Post(string id)
        {
            return new ContentResult { Content = id };
        }*/
    }
}
