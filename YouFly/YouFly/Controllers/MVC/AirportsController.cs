using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using YouFly.Data;
using YouFly.Models;

namespace YouFly.Controllers
{
    public class AirportsController : Controller
    {
        private readonly FlightContext _context;

        public AirportsController(FlightContext context)
        {
            _context = context;    
        }

        // GET: Airports
        public async Task<IActionResult> Index(string airportIATA,string searchName)
        {
            // Use LINQ to get list of IATA.
            IQueryable<string> iataQuery = from a in _context.Airports
                                            orderby a.IATA
                                            select a.IATA;

            var airports = from a in _context.Airports
                         select a;

            if (!String.IsNullOrEmpty(searchName))
            {
               airports = airports.Where(s => s.Name.Contains(searchName));
            }
            if (!String.IsNullOrEmpty(airportIATA))
            {
                airports = airports.Where(x => x.IATA == airportIATA);
            }
            var airportIATAVM = new AirportIATAViewModel();
            airportIATAVM.iata = new SelectList(await iataQuery.Distinct().ToListAsync());
            airportIATAVM.airports = await airports.ToListAsync();

            return View(airportIATAVM);

            
        }

        [HttpPost]
        public string Index(string searchString, bool notUsed)
        {
            return "From [HttpPost]Index: filter on " + searchString;
        }

        // GET: Airports/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var airport = await _context.Airports
                .SingleOrDefaultAsync(m => m.ID == id);
            if (airport == null)
            {
                return NotFound();
            }

            return View(airport);
        }

        // GET: Airports/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Airports/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,City,Country,IATA,ICAO,Latitude,Longtitude,Altitude,UTC,DST,TimeZone,Type,Source")] Airport airport)
        {
            if (ModelState.IsValid)
            {
                _context.Add(airport);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(airport);
        }

        // GET: Airports/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var airport = await _context.Airports.SingleOrDefaultAsync(m => m.ID == id);
            if (airport == null)
            {
                return NotFound();
            }
            return View(airport);
        }

        // POST: Airports/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,City,Country,IATA,ICAO,Latitude,Longtitude,Altitude,UTC,DST,TimeZone,Type,Source")] Airport airport)
        {
            if (id != airport.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(airport);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AirportExists(airport.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(airport);
        }

        // GET: Airports/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var airport = await _context.Airports
                .SingleOrDefaultAsync(m => m.ID == id);
            if (airport == null)
            {
                return NotFound();
            }

            return View(airport);
        }

        // POST: Airports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var airport = await _context.Airports.SingleOrDefaultAsync(m => m.ID == id);
            _context.Airports.Remove(airport);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool AirportExists(int id)
        {
            return _context.Airports.Any(e => e.ID == id);
        }
    }
}
