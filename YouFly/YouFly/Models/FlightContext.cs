﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace YouFly.Models
{
    public class FlightContext : DbContext
    {
        public FlightContext(DbContextOptions<FlightContext> options) : base(options)
        {

        }
        public DbSet<Flights> Flights { get; set; }
        public DbSet<UserAccount> UserAccounts { get; set; }
        public DbSet<Airport> Airports { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //inventory stuff
            modelBuilder.Entity<Flights>()
                .HasAlternateKey(i => i.ID)
                .HasName("UniqueKey_FlightID");

            modelBuilder.Entity<Airport>().HasAlternateKey(i => i.ID).HasName("UniqueKey_AirportID");
                ;
            //user stuff
            modelBuilder.Entity<UserAccount>()
                .HasAlternateKey(u => u.UserID)
                .HasName("UniqueKey_UserID");

            modelBuilder.Entity<UserAccount>()
                .HasAlternateKey(u => u.UserName)
                .HasName("UniqueKey_Username");
        }
    }
}