﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouFly.Models
{
    public class Route
    {
        public List<string> allIATA { get; set; }
        public string IATAF { get; set; }
        public string IATAT { get; set; }
        public string Date { get; set; }
        public string FlightTime { get; set; }
        public float Distance { get; set; }
        public int FCSeats { get; set; }
        public int RegSeats { get; set; }
        public int PriceFC{get;set;}
        public int Price { get; set; }
        public int ID { get; set; }
    }
}
