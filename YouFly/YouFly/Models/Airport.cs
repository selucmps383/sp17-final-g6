﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using YouFlyLib;

namespace YouFly.Models
{
    public class Airport
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.None)]

        public int ID { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string IATA { get; set; }
        public string ICAO { get; set; }
        public float Latitude{ get; set; }
        public float Longtitude { get; set; }
        public float Altitude { get; set; }
        public float UTC { get; set; }
        public string DST { get; set; }
        public string TimeZone { get; set; }
        public string Type { get; set; }
        public string Source { get; set; }
    }

    
}
