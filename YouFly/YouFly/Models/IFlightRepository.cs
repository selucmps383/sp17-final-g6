﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouFly.Models
{
    public interface IFlightRepository
    {
        void Add(Flights Location);
        IEnumerable<Flights> GetAll();
        Flights Find(long key);
        void Remove(long key);
        void Update(Flights Location);
    }
}
