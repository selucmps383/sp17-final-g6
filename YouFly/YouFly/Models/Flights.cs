﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace YouFly.Models
{
    public class Flights
    {   [Key]
        public int ID { get; set; }
        public int Price { get; set; }
        public int PriceFC { get; set; }
        public int TotalFC { get; set; }
        public int RemainingFC { get; set; }
        public int TotalSeat { get; set; }
        public int RemaingSeat { get; set; }
        public string Date { get; set; }
        public string time { get; set; }
        public int Distance { get; set; }
        public string From { get; set; }
        public string Destination { get; set; }
    }
}
