﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouFly.Models
{
    public class AirportIATAViewModel
    {
        public List<Airport> airports;
        public SelectList iata;
        public string airportIATA { get; set; }
    }
}
