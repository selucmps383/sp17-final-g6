﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouFly.Models
{
    public class FlightRepository : IFlightRepository
    {
        private readonly FlightContext _context;

        public FlightRepository(FlightContext context)
        {
            _context = context;

            
                
        }

        public IEnumerable<Flights> GetAll()
        {
            return _context.Flights.ToList();
        }

        public void Add(Flights Location)
        {
            _context.Flights.Add(Location);
            _context.SaveChanges();
        }

        public Flights Find(long key)
        {
            return _context.Flights.FirstOrDefault(t => t.ID == key);
        }

        public void Remove(long key)
        {
            var entity = _context.Flights.First(t => t.ID == key);
            _context.Flights.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(Flights Location)
        {
            _context.Flights.Update(Location);
            _context.SaveChanges();
        }
    }
}
