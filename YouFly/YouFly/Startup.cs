﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using YouFly.Data;
using Microsoft.EntityFrameworkCore;
using YouFly.Models;
using System.IO;
using CsvHelper;
using YouFly.Controllers.API;

namespace YouFly
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);
            var connection = @"Server=(localdb)\mssqllocaldb;Database=Flights2;Trusted_Connection=True;";
            services.AddDbContext<FlightContext>(options => options.UseSqlServer(connection));
            services.AddScoped<IAirportRepository, AirportRepository>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var dbContext = serviceScope.ServiceProvider.GetService<FlightContext>();
                dbContext.Database.Migrate();

                if (!dbContext.UserAccounts.Any())
                {
                    dbContext.UserAccounts.Add(new UserAccount
                    {
                        UserName = "admin",
                        Name = "Niel",
                        Email = "Admin@selu.edu",
                        Password = "Selu2017"
                    });
                    dbContext.SaveChanges();
                }

                if (!dbContext.Airports.Any())
                {
                    TextReader reader = File.OpenText("Data/airports.csv");

                    //var airports = new List<Airport>();
                    try
                    {
                        var csv = new CsvReader(reader);
                        csv.Configuration.Delimiter = ",";
                        var airports = csv.GetRecords<Airport>();

                        foreach (Airport ap in airports)
                        {
                            dbContext.Airports.Add(ap);
                        }
                    }
                    catch (CsvHelperException ex)
                    {
                        Console.WriteLine(ex.Data.Values);
                    }
                    dbContext.SaveChanges();
                }

                if (!dbContext.Flights.Any())
                {
                    dbContext.SaveChanges();
                }
            }


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            
        }
    }
}
