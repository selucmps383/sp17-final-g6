﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace YouFly.Migrations
{
    public partial class MFM : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Airports",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    Altitude = table.Column<float>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    DST = table.Column<string>(nullable: true),
                    IATA = table.Column<string>(nullable: true),
                    ICAO = table.Column<string>(nullable: true),
                    Latitude = table.Column<float>(nullable: false),
                    Longtitude = table.Column<float>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Source = table.Column<string>(nullable: true),
                    TimeZone = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    UTC = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("UniqueKey_AirportID", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "UserAccounts",
                columns: table => new
                {
                    UserID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Email = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("UniqueKey_UserID", x => x.UserID);
                    table.UniqueConstraint("UniqueKey_Username", x => x.UserName);
                });

            migrationBuilder.CreateTable(
                name: "Flights",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<string>(nullable: true),
                    Destination = table.Column<string>(nullable: true),
                    Distance = table.Column<int>(nullable: false),
                    From = table.Column<string>(nullable: true),
                    Price = table.Column<int>(nullable: false),
                    PriceFC = table.Column<int>(nullable: false),
                    RemaingSeat = table.Column<int>(nullable: false),
                    RemainingFC = table.Column<int>(nullable: false),
                    TotalFC = table.Column<int>(nullable: false),
                    TotalSeat = table.Column<int>(nullable: false),
                    UserAccountUserID = table.Column<int>(nullable: true),
                    time = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("UniqueKey_FlightID", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Flights_UserAccounts_UserAccountUserID",
                        column: x => x.UserAccountUserID,
                        principalTable: "UserAccounts",
                        principalColumn: "UserID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Flights_UserAccountUserID",
                table: "Flights",
                column: "UserAccountUserID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Airports");

            migrationBuilder.DropTable(
                name: "Flights");

            migrationBuilder.DropTable(
                name: "UserAccounts");
        }
    }
}
