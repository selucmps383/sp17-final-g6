﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using YouFly.Models;

namespace YouFly.Migrations
{
    [DbContext(typeof(FlightContext))]
    partial class FlightContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("YouFly.Models.Airport", b =>
                {
                    b.Property<int>("ID");

                    b.Property<float>("Altitude");

                    b.Property<string>("City");

                    b.Property<string>("Country");

                    b.Property<string>("DST");

                    b.Property<string>("IATA");

                    b.Property<string>("ICAO");

                    b.Property<float>("Latitude");

                    b.Property<float>("Longtitude");

                    b.Property<string>("Name");

                    b.Property<string>("Source");

                    b.Property<string>("TimeZone");

                    b.Property<string>("Type");

                    b.Property<float>("UTC");

                    b.HasKey("ID")
                        .HasName("UniqueKey_AirportID");

                    b.ToTable("Airports");
                });

            modelBuilder.Entity("YouFly.Models.Flights", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Date");

                    b.Property<string>("Destination");

                    b.Property<int>("Distance");

                    b.Property<string>("From");

                    b.Property<int>("Price");

                    b.Property<int>("PriceFC");

                    b.Property<int>("RemaingSeat");

                    b.Property<int>("RemainingFC");

                    b.Property<int>("TotalFC");

                    b.Property<int>("TotalSeat");

                    b.Property<int?>("UserAccountUserID");

                    b.Property<string>("time");

                    b.HasKey("ID")
                        .HasName("UniqueKey_FlightID");

                    b.HasIndex("UserAccountUserID");

                    b.ToTable("Flights");
                });

            modelBuilder.Entity("YouFly.Models.UserAccount", b =>
                {
                    b.Property<int>("UserID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Password")
                        .IsRequired();

                    b.Property<string>("UserName")
                        .IsRequired();

                    b.HasKey("UserID")
                        .HasName("UniqueKey_UserID");

                    b.HasAlternateKey("UserName")
                        .HasName("UniqueKey_Username");

                    b.ToTable("UserAccounts");
                });

            modelBuilder.Entity("YouFly.Models.Flights", b =>
                {
                    b.HasOne("YouFly.Models.UserAccount")
                        .WithMany("Tickets")
                        .HasForeignKey("UserAccountUserID");
                });
        }
    }
}
