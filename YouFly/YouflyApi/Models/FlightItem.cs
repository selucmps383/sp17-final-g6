﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;



namespace YouflyApi.Models
{
    public class FlightItem
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Key { get; set; }
        public int ID { get; set; }
        public int Price { get; set; }
        public string SeatList { get; set; }
        public string Location { get; set; }
    }
}
