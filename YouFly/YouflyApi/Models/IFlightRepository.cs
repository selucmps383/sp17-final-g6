﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace YouflyApi.Models
{
    public interface IFlightRepository
    {
        void Add(FlightItem Location);
        IEnumerable<FlightItem> GetAll();
        FlightItem Find(long key);
        void Remove(long key);
        void Update(FlightItem Location);
    }
}
