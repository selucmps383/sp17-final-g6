﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace YouflyApi.Models
{
    public class FlightRepository : IFlightRepository
    {
        private readonly FlightContext _context;

        public FlightRepository(FlightContext context)
        {
            _context = context;

            if (_context.FlightItems.Count() == 0)
                Add(new FlightItem { Location = "City1" });
        }

        public IEnumerable<FlightItem> GetAll()
        {
            return _context.FlightItems.ToList();
        }

        public void Add(FlightItem Location)
        {
            _context.FlightItems.Add(Location);
            _context.SaveChanges();
        }

        public FlightItem Find(long key)
        {
            return _context.FlightItems.FirstOrDefault(testc => testc.Key == key);
        }

        public void Remove(long key)
        {
            var entity = _context.FlightItems.First(t => t.Key == key);
            _context.FlightItems.Remove(entity);
            _context.SaveChanges();
        }

        public void Update(FlightItem Location)
        {
            _context.FlightItems.Update(Location);
            _context.SaveChanges();
        }
    }
}
