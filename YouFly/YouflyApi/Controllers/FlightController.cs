﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using YouflyApi.Models;

namespace YouflyApi.Controllers
{
    [Route("api/[controller]")]
    public class FlightController : Controller
    {
        private readonly IFlightRepository _flightRepository;

        public FlightController(IFlightRepository flightRepository)
        {
            _flightRepository = flightRepository;
        }

        [HttpGet]
        public IEnumerable<FlightItem> GetAll()
        {
            return _flightRepository.GetAll();
        }

        [HttpGet("{id}", Name = "GetFlights")]
        public IActionResult GetById(long id)
        {
            var item = _flightRepository.Find(id);
            if (item == null)
            {
                return NotFound();
            }

            return new ObjectResult(item);
        }
    }
}
