﻿export class Flights {
    ID: number;
    Price: number;
    RegSeats: number;
    FCSeats: number;
    Location: string;
    Destination: string;
}
