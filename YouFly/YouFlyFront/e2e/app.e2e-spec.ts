import { YouFlyPage } from './app.po';

describe('you-fly App', function() {
  let page: YouFlyPage;

  beforeEach(() => {
    page = new YouFlyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
