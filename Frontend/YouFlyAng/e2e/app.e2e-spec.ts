import { YouFlyAngPage } from './app.po';

describe('you-fly-ang App', () => {
  let page: YouFlyAngPage;

  beforeEach(() => {
    page = new YouFlyAngPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
