﻿import { TestBed, inject } from '@angular/core/testing';

import { FlightDataService } from './flight-data.service';

import { Flights } from './flights';

describe('FlightDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FlightDataService]
    });
  });

  it('should ...', inject([FlightDataService], (service: FlightDataService) => {
    expect(service).toBeTruthy();
  }));

  describe('#getAllFlights()', () => {

      it('should return an empty array by default', inject([FlightDataService], (service: FlightDataService) => {
          expect(service.getAllFlights()).toEqual([]);
      }));

      it('should return all todos', inject([FlightDataService], (service: FlightDataService) => {
          let flight1 = new Flights({ Price: '$400', SeatList: '4', Location: 'New Orleans', Destination: 'Seattle' });
          let flight2 = new Flights({ Price: '$200', SeatList: '2', Location: 'New Orleans', Destination: 'Dallas' });
          service.addFlights(flight1);
          service.addFlights(flight2);
          expect(service.getAllFlights()).toEqual([flight1, flight2]);
      }));

  });
});
