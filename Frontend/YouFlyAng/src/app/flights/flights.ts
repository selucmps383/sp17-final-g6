﻿export class Flights {
    ID: number;
    Price: number;
    SeatList: string;
    Location: string;
    Destination: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
