﻿import { Component, Input, OnInit, OnDestroy } from '@angular/core';

import { Flights } from './flights';

@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.css']
})
export class FlightsComponent implements OnInit, OnDestroy{


    private sub: any;
    flights: Flights[];

  //constructor() { }

    ngOnInit() {

    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

}
