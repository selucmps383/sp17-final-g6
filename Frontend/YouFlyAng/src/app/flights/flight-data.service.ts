﻿import { Injectable } from '@angular/core';
import { Flights } from './flights';

@Injectable()
export class FlightDataService {

    lastId: number = 0;

    //placeholder for flights
    flights: Flights[];

    constructor() { }

    addFlights(flights: Flights): FlightDataService {
        if (!flights.ID) {
            flights.ID = ++this.lastId;
        }
        this.flights.push(flights);
        return this;
    }


    // Simulate GET /flights
    getAllFlights(): Flights[] {
        return this.flights;
    }

}
