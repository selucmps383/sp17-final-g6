﻿export class Airport {
        AirportID: number; 
        AirportName: string; 
        AirportCity: string; 
        AirportCountry: string; 
        AirportCode: string; 
        AirportFCode: string; 
        AirportLong : number;
        AirportLat: number;
        AirportUnknown: number;
        AirportA: string; 
        AirportTimezone: string; 
        AirportType: string;
        AirportYouFly: string; 
}
