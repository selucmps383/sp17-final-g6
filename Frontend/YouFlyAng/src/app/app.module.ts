﻿import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AirportComponent } from './airport/airport.component';
import { FlightsComponent } from './flights/flights.component';
import { PassengerComponent } from './passenger/passenger.component';
import { RouteComponent } from './route/route.component';
import { TicketComponent } from './ticket/ticket.component';
import { UserComponent } from './user/user.component';


@NgModule({
  declarations: [
    AppComponent,
    AirportComponent,
    FlightsComponent,
    PassengerComponent,
    RouteComponent,
    TicketComponent,
    UserComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
