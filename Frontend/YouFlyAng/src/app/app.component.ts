﻿import { Component } from '@angular/core';
import { FlightsComponent } from './flights/flights.component';


@Component({
    selector: 'app-root',
    template: `<h1>{{title}}</h1>
                <div>
                    <app-flights></app-flights>
                </div>`,
    entryComponents: [FlightsComponent],
    // just replaced "directives" with "entryComponents" as directives are deprecated
  //templateUrl: './app.component.html',
   //styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Welcome to YouFly!';
}
